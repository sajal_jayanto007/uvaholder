import React from 'react'


import { ColumnChart } from "@opd/g2plot-react";
import { StepLine } from '@ant-design/charts';

import { Card, Col, Row } from 'antd';


const VerdictGrapg = ( { verdictCount , userName , userUname , lineGraphData} ) => {

    const data = [
        {
            Verdict: 'AC',
            Count: verdictCount.accepted,
        },
        {
            Verdict: 'PE',
            Count: verdictCount.presentationError,
        },
        {
            Verdict: 'WA',
            Count: verdictCount.wrongAnswer,
        },
        {
            Verdict: 'TL',
            Count: verdictCount.timeLimit,
        },
        {
            Verdict: 'ML',
            Count: verdictCount.memoryLimit,
        },
        {
            Verdict: 'CE',
            Count: verdictCount.compileError,
        },
        {
            Verdict: 'RE',
            Count: verdictCount.runtimeError,
        },
        {
            Verdict: 'OT',
            Count: verdictCount.outputLimit,
        },
        
    ];
    const config = {
        title: {
          visible: true,
          text: 'Problem Submission Statistics',
        },
        description: {
          visible: true,
          text: `${userName} - ${userUname}`,
        },
        data,
        xField: 'Verdict',
        yField: 'Count',
        colorField: 'Verdict',
        color: ['#228B22', '#666600', '#FF0000', '#0000FF', '#0000AA', '#AAAA00', '#00CED1', '#000000'],
        label: {
            visible: true,
            position: 'top',
            adjustColor: true,
        },
        
    };

    const configline = {
        title: {
          visible: true,
          text: 'Progress Over The Years',
        },
        description: {
          visible: true,
          text: `${userName} - ${userUname}`,
        },
        padding: 'auto',
        forceFit: true,
        data : lineGraphData,
        xField: 'year',
        yField: 'solve',
        smooth: false,
        label: {
            visible: true,
            type: 'point',
        },
        point: {
            visible: true,
            size: 2,
            shape: 'diamond',
            style: {
                fill: 'white',
                stroke: '#2593fc',
                lineWidth: 2,
            },
        },
    };


    return (
        <div className="site-card-wrapper">
            <Row gutter={[32, 8]}>
                <Col span={12} > <Card size='small' bordered={false}> <ColumnChart { ...config } /> </Card> </Col>
                <Col span={12} > <Card size='small' bordered={false}> <StepLine { ...configline } />  </Card> </Col>
            </Row>
        </div>
    )
}

export default VerdictGrapg
