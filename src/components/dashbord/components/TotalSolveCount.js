import React from 'react'

import { Card, Col, Row } from 'antd';

const TotalSolveCount = ({ totalSolve ,  totalSubmissions }) => {

    return (
        <div className="site-card-wrapper">
            <Row gutter={[32, 8]}>
                <Col span={12} > 
                    <Card title="Total Solved Probelms" bordered={false}   
                        headStyle={{ 
                            background: '#1890ff' , 
                            textTransform: 'uppercase' , 
                            fontWeight: 'bold',
                            color: '#fff'
                        }} bodyStyle={{ fontSize : '25px' , fontWeight: 'bold' }}>
                        {totalSolve}  
                    </Card> 
                </Col>
                <Col span={12} > 
                    <Card title="Total Submitted " bordered={false}  
                        headStyle={{ 
                            background: '#1890ff' ,  
                            textTransform: 'uppercase' , 
                            fontWeight: 'bold',
                            color: '#fff'
                        }} bodyStyle={{ fontSize : '25px' , fontWeight: 'bold'}}> 
                        {totalSubmissions}
                    </Card> 
                </Col>
            </Row>
        </div>
    )
}

export default TotalSolveCount
