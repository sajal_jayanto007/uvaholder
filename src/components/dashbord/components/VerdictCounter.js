import React from 'react'

import { Card, Col, Row } from 'antd';


const VerdictCounter = ({ verdictCount }) => {

    return (
        <div className="site-card-wrapper">
            <Row gutter={[8, 8]}>
                <Col span={3}>
                    <Card title="Accepted"  bordered={false} 
                        
                        headStyle={{ 
                            background: '#1890ff' , 
                            color: '#fff' , 
                            
                            
                        }} bodyStyle={{ fontSize : '20px' ,  }}
                    > 
                        {verdictCount.accepted}
                    </Card>
                </Col>
                <Col span={3}>
                    <Card title="Wrong answer"   bordered={false} 
                        
                        headStyle={{ 
                            background: '#1890ff' , 
                            color: '#fff', 
                            
                            
                        }} bodyStyle={{ fontSize : '20px' ,  }}
                    > 
                        {verdictCount.wrongAnswer}
                    </Card>
                </Col>
                <Col span={3}>
                    <Card title="Time limit"  bordered={false} 
                    headStyle={{ background: '#1890ff' , color: '#fff' ,  }}
                    bodyStyle={{ fontSize : '20px' ,  }}
                    > 
                        {verdictCount.timeLimit}
                    </Card>
                </Col>
                <Col span={3}>
                    <Card title="Runtime error"  bordered={false} 
                    headStyle={{ background: '#1890ff' , color: '#fff' , }}
                    bodyStyle={{ fontSize : '20px'  , }}
                    > 
                        {verdictCount.runtimeError} 
                    </Card>
                </Col>
                <Col span={3}>
                    <Card title="Compile error"   bordered={false} 
                    headStyle={{ background: '#1890ff' , color: '#fff' , }}
                    bodyStyle={{ fontSize : '20px' ,  }}
                    > 
                        {verdictCount.compileError}
                    </Card>
                </Col>
                <Col span={3}>
                    <Card title="Memory limit"  bordered={false} 
                    headStyle={{ background: '#1890ff' , color: '#fff' ,  }}
                    bodyStyle={{ fontSize : '20px' , }}
                    > 
                        {verdictCount.memoryLimit}
                    </Card>
                </Col>
                <Col span={3}>
                    <Card title="Presentation error"  bordered={false} 
                    headStyle={{ background: '#1890ff' , color: '#fff' , }}
                    bodyStyle={{ fontSize : '20px' ,  }}
                    > 
                        {verdictCount.presentationError}
                    </Card>
                </Col>
                <Col span={3}>
                    <Card title="Output limit"   bordered={false} 
                    headStyle={{ background: '#1890ff' , color: '#fff' , }}
                    bodyStyle={{ fontSize : '20px' , }}
                    > 
                        {verdictCount.outputLimit}
                    </Card>
                </Col>
            </Row>

            

        </div>  
    )
}

export default VerdictCounter
