import React , { useState,  useEffect } from 'react'

import VerdictCounter from './components/VerdictCounter';
import VerdictGrapg from './components/VerdictGrapg';
import TotalSolveCount from './components/TotalSolveCount';

import axios from 'axios';

const Dashbord = () => {
    
    const [ userName , setUserName ] = useState("");
    const [ userUname , setUserUname ] = useState("");
    const [ verdictCount  , setverdictCount ] = useState({});
    const [ lineGraphData , setlineGraphData ] = useState([]);
    const [ totalSolve , setTotalSolve ] = useState(0);
    const [ totalSubmissions , setTotalSubmissions ] = useState(0); 


    useEffect(() => {

        const fatchSubmissions = async (userId) => {

            const response = await axios.get(`https://uhunt.onlinejudge.org/api/subs-user/${userId}`); 

            setUserName(response.data.name);
            setUserUname(response.data.uname);

            let countVerdict = {
                accepted : 0,
                wrongAnswer : 0,
                timeLimit : 0,
                runtimeError : 0,
                compileError : 0,
                memoryLimit : 0,
                presentationError : 0,
                outputLimit : 0
            }

            let lineGraph = [];
            let yearAcceptedCount = {};
            let minYear = 300000;
            let maxYear = 0;
            let nowYear = new Date().getFullYear();
            let uniqueSolveProblemList = new Set();

            response.data.subs.forEach(sub => {

                switch(sub[2]) {

                    case 90: countVerdict.accepted++; break; 
                    case 70: countVerdict.wrongAnswer++; break; 
                    case 50: countVerdict.timeLimit++; break;
                    case 40: countVerdict.runtimeError++; break;
                    case 30: countVerdict.compileError++; break; 
                    case 60: countVerdict.memoryLimit++; break; 
                    case 80: countVerdict.presentationError++; break; 
                    case 45: countVerdict.outputLimit++; break; 
                    default : break; 
                }

                let submissionYear  = new Date(sub[4] * 1000).getFullYear();
                
                minYear = Math.min(minYear , submissionYear);
                maxYear = Math.max(maxYear , submissionYear);

                if(sub[2] === 90 && !uniqueSolveProblemList.has(sub[1]) ){
                    
                    if(!yearAcceptedCount.hasOwnProperty(submissionYear)) yearAcceptedCount[submissionYear] = 1;
                    else yearAcceptedCount[submissionYear]++;

                    uniqueSolveProblemList.add(sub[1]);
                }

            });

            yearAcceptedCount[minYear - 1] = 0;
            
            for(let i = maxYear + 1 ; i <= nowYear ; ++i) yearAcceptedCount[i] = 0;
           
            for (let key in yearAcceptedCount) lineGraph.push({ year : key , solve : yearAcceptedCount[key] })
            
            for(let i = 1 ; i < lineGraph.length ; ++i) lineGraph[i].solve += lineGraph[i - 1].solve;

            setverdictCount(countVerdict);
            setlineGraphData(lineGraph);
            setTotalSubmissions(response.data.subs.length);
            setTotalSolve(uniqueSolveProblemList.size);

        }

        fatchSubmissions(313608);
        
    }, []);

    return (
        <div>
            
            <TotalSolveCount totalSolve={totalSolve} totalSubmissions={totalSubmissions} />  
            
            <VerdictCounter verdictCount={verdictCount} /> 
            
            <VerdictGrapg verdictCount={verdictCount} userName={userName} userUname={userUname} lineGraphData={lineGraphData} />
            
        </div>
    )
}

export default Dashbord
