import React , { useState, useEffect } from 'react'

import { Table , Tag, Card , Input } from 'antd';

import axios from 'axios';


const { Search } = Input;

const Allsubmission = ({ allProblemList }) => {

    const [ userName , setUserName ] = useState("");
    const [ userUname , setUserUname ] = useState("");
    const [ submissionList , setSubmissionList ] = useState([]);
    const [ tableData , setTableData ] = useState([]);
    const [ tableDataloding , setTableDataloding ] = useState(true);


    
    const getVerdict = (verdictId) => {

        switch(verdictId) {

            case 10: return "Submission error";  
            case 15: return "Can't be judged";
            case 20: return "In queue"; 
            case 30: return "Compile error"; 
            case 35: return "Restricted function"; 
            case 40: return "Runtime error"; 
            case 45: return "Output limit"; 
            case 50: return "Time limit"; 
            case 60: return "Memory limit"; 
            case 70: return "Wrong answer"; 
            case 80: return "PresentationE"; 
            case 90: return "Accepted"; 
            default : return "--------"; 
        }

    };

    const getLanguage = (languageId) => {

        switch(languageId) {

            case 1: return "ANSI C";  
            case 2: return "Java";
            case 3: return "C++"; 
            case 4: return "Pascal"; 
            case 5: return "C++11"; 
            case 6: return "Python"; 
            default : return "--------"; 
        }
    }

    const timeConverter = (timestamp) => {
        
        let a = new Date(timestamp * 1000);
        let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        let year = a.getFullYear();
        let month = months[a.getMonth()];
        let date = a.getDate();
        let hour = a.getHours();
        let min = a.getMinutes();
        let sec = a.getSeconds();
        let time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec ;
        
        return time;
    };
    
    
    useEffect(() => {
        
        const findProblemByProblemId = (problemId) => { 

            let low = 0 ;
            let high = allProblemList.length;
            let mid = 0;
    
            while(low <= high){
    
                mid = (low + high) >> 1;
    
                if(allProblemList[mid][0] === problemId) return mid;
                else if(allProblemList[mid][0] < problemId)low = mid + 1;
                else high = mid - 1;
            }
    
            return 0;
        };

        const fatchSubmissions = async (userId , solveCount) => {
            
            const response = await axios.get(`https://uhunt.onlinejudge.org/api/subs-user-last/${userId}/${solveCount}`); 

            setUserName(response.data.name);
            setUserUname(response.data.uname);

            let dataSubmission = [];

            response.data.subs.forEach(sub => {
                
                let index = findProblemByProblemId(sub[1]);

                let eachSubmission = {

                    key : sub[0],
                    problemNumber : `${allProblemList[index][1]}`,
                    numberWithName : `${allProblemList[index][1]} - ${allProblemList[index][2]}`,
                    verdict : getVerdict(sub[2]),
                    language : getLanguage(sub[5]),
                    runtime : (sub[3] / 1000).toFixed(3),
                    bestRuntime : (allProblemList[index][4] / 1000).toFixed(3),
                    rank : sub[6] !== -1 ? sub[6] : '-',
                    submissionTime : timeConverter(sub[4])
                }

                dataSubmission.unshift(eachSubmission);
                
            });

            setSubmissionList(dataSubmission);
            setTableData(dataSubmission);
            setTableDataloding(false);
        }

        if(allProblemList.length > 0) fatchSubmissions(339 , 10000000);

    }, [allProblemList]);

    const searchKeyWordType = (value) =>{
        setTableDataloding(true);
        setTableData(submissionList.filter(eachrow => eachrow.problemNumber.startsWith(value)));
        setTableDataloding(false);
    }

    const searchButtonPress = (value) => {
        setTableDataloding(true);
        setTableData(submissionList.filter(eachrow => eachrow.problemNumber === value));
        setTableDataloding(false);
    }

    const columns = [
        {
            title: "Problem",
            dataIndex: "numberWithName",
            key: "numberWithName",
            render: text => <span>{text}</span>
        },
        {
            title: "Verdict",
            key: "verdict",
            dataIndex: "verdict",
            render: verdict => (
            <>    
                { 
                    <Tag color= 
                        { 
                            verdict === 'Accepted'? '#228B22' : 
                            verdict === 'PresentationE'? '#666600' :
                            verdict === 'Wrong answer'? '#FF0000' :
                            verdict === 'Time limit'? '#0000FF' :
                            verdict === 'Memory limit'? '#0000AA' :
                            verdict === 'Compile error'? '#AAAA00' :
                            verdict === 'Runtime error'? '#00CED1' :
                            verdict === 'Output limit'? '#000000' : '#000000'

                        } key={verdict}>

                        {verdict.toUpperCase()}
                    </Tag>
                }
            </>
            )
        },
        {
            title: "Language",
            dataIndex: "language",
            key: "language"
        },
        {
            title: "Time",
            dataIndex: "runtime",
            key: "runtime"
        },
        {
            title: "Best",
            dataIndex: "bestRuntime",
            key: "bestRuntime"
        },
        {
            title: "Rank",
            dataIndex: "rank",
            key: "rank"
        },
        {
            title: "Submission Time",
            dataIndex: "submissionTime",
            key: "submissionTime"
        }
    ];
    
    return (
        <div>
            <Card title={`Last Submissions : ${userName} ( ${userUname} )`} 
                extra={ 
                    <Search type='text' placeholder="Problem Number" 
                        onChange={(e) => searchKeyWordType(e.target.value)  } 
                        onSearch={ value => searchButtonPress(value) }
                        style={{ width: 200 }} 
                    /> 
                }>
                
                <Table loading={tableDataloding} columns={columns} dataSource={tableData} bordered size='small' pagination={{pageSize: 15}} />
            </Card>
        </div>
    );
}

export default Allsubmission
