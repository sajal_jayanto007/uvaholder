import React , { useState , useEffect } from 'react'

import axios from 'axios';

import { Collapse , Card , Divider , List } from "antd";

const { Panel  } = Collapse;


function callback(key) {
    console.log(key);
}

export const Contest = ({ allProblemList }) => {


    const [ contestlist , SetContestList ] = useState([]);

    

    useEffect(() => {

        let ProblemsList = [];


        const findProblemByProblemNumber = (problemNumber) => { 

            let low = 0 ;
            let high = ProblemsList.length;
            let mid = 0;

            while(low <= high){

                mid = (low + high) >> 1;

                if(ProblemsList[mid][1] === problemNumber) return mid;
                else if(ProblemsList[mid][1] < problemNumber) low = mid + 1;
                else high = mid - 1;
            }

            return -1;
        };

        ProblemsList = allProblemList;

        ProblemsList.sort((one , two) => {
            return one[1] - two[1];
        })

        const fatchContest = async () => {

            const request = await axios.get(`https://uhunt.onlinejudge.org/api/contests`);

            const data = [];

            for(let i = 0 ; i < request.data.length ; ++i){
                
                let list = [];
                for(let j = 0 ; j < request.data[i].problems.length ; ++j){

                    let number = request.data[i].problems[j];
                    let index = findProblemByProblemNumber(number);

                    if(index == -1) list.push(`${number} - -------?---------`);
                    else list.push(`${number} - ${ProblemsList[index][2]}`);
                }

                let datalist = {
                    name : request.data[i].name,
                    problems : list
                }

                data.push(datalist);

            }


            SetContestList(data);
        } 

        if(allProblemList.length > 0) fatchContest();

    }, [allProblemList]);


    return (
        <>
            <Card bordered>
                <Divider orientation="left"> UVa OJ Past Contests </Divider>
                <Collapse bordered onChange={callback}>
                    
                    {   
                        contestlist.map( (contest ) => 
                            <Panel header={contest.name} key={contest.id} style={{paddingBottom: 2}}>
                                <List dataSource={contest.problems} renderItem =
                                    { 
                                        problem => 
                                        <List.Item> 
                                            {problem}
                                        </List.Item>
                                    } 
                                />
                            </Panel>
                        )
                    }

                </Collapse>
            </Card>
        </>
    );
}

export default Contest;