import React from 'react'
import { ColumnChart } from "@opd/g2plot-react";

const ProblemVerdictStatics = ({ verdictCount  , problemNumber , problemName }) => {
    
    const data = [
        {
            Verdict: 'AC',
            Count: verdictCount.accepted,
        },
        {
            Verdict: 'PE',
            Count: verdictCount.presentationError,
        },
        {
            Verdict: 'WA',
            Count: verdictCount.wrongAnswer,
        },
        {
            Verdict: 'TL',
            Count: verdictCount.timeLimit,
        },
        {
            Verdict: 'ML',
            Count: verdictCount.memoryLimit,
        },
        {
            Verdict: 'CE',
            Count: verdictCount.compileError,
        },
        {
            Verdict: 'RE',
            Count: verdictCount.runtimeError,
        },
        {
            Verdict: 'OT',
            Count: verdictCount.outputLimit,
        },
        
    ];
    const config = {
        title: {
          visible: true,
          text: `${problemNumber} - ${problemName}`,
        },
        description: {
          visible: true,
          text: 'Problem Statistics',
        },
        data,
        xField: 'Verdict',
        yField: 'Count',
        colorField: 'Verdict',
        color: ['#228B22', '#666600', '#FF0000', '#0000FF', '#0000AA', '#AAAA00', '#00CED1', '#000000'],
        label: {
            visible: true,
            position: 'top',
            adjustColor: true,
        },
        
    };

    return (
        <ColumnChart { ...config } />
    )
}

export default ProblemVerdictStatics
