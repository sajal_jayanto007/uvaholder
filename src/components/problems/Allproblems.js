import React , { useState , useEffect } from 'react';
import ProblemVerdictStatics from './ProblemVerdictStatics';

import axios from 'axios';

import { Card , Input  , Table , Tag , Row , Col , Typography , List , Divider } from 'antd';

const { Search  } = Input;

const { Title , Text } = Typography;

 
const Allproblems = () => {
    
    const [ searechProblemNumber , setsearechProblemNumber ] = useState(); 
    const [ verdictCount , setVerdictCount ] = useState([]);
    const [ problemNumber , setProblemNumber ] = useState(0);
    const [ problemName , setProblemName ] = useState("");
    const [ showResutl , setShowResutl] = useState(false);
    const [ mySubmissionTable , setMySubmissionTable ] = useState([]);
    const [ lastFiveDaySubmission , setLastFiveDaySubmission ] = useState([]); 
    const [ bestSubmissionTable , setbestSubmissionTable ] = useState([]);
    const [ solveByUser , setSolveByUser ] = useState(0);
    const [ problemTime , setProblemTime ] = useState(0);
    const [ problemStatus , setProblemStatus] = useState("");
    const [ bestSolveTime , setBestSolveTime ] = useState(0);


  
    
    useEffect(() => {

        
        
        const fatchProblemData = async (userId , problemNumber) => {

            const response = await axios.get(`https://uhunt.onlinejudge.org/api/p/num/${problemNumber}`); 
            
            setProblemNumber(response.data.num);
            setProblemName(response.data.title);
            setSolveByUser(response.data.dacu);
            setProblemTime((response.data.rtl / 1000).toFixed(3)); 
            setBestSolveTime((response.data.mrun / 1000).toFixed(3));
            setProblemStatus(response.data.status === 0 ? 'Unavailable' :  response.data.status === 1 ? 'Normal' : 'Special judge' )
            
            
            let countVerdict = {
                accepted : response.data.ac,
                wrongAnswer : response.data.wa,
                timeLimit : response.data.tle,
                runtimeError : response.data.re,
                compileError : response.data.ce,
                memoryLimit : response.data.mle,
                presentationError : response.data.pe,
                outputLimit : response.data.ole + response.data.sube + response.data.noj + response.data.inq
            }

            setVerdictCount(countVerdict);

            //// Second request ////
            const response2 = await axios.get(`https://uhunt.onlinejudge.org/api/subs-nums/${userId}/${problemNumber}/0`);

            const mySubmissiondata = [];

            response2.data[userId].subs.forEach(sub => {

                let eachSubmission = {
                    key : sub[0],
                    submissionsId : sub[0],
                    rank : sub[6] !== -1 ? sub[6] : '-',
                    userName : `${response2.data[userId].name}(${response2.data[userId].uname})`,
                    verdict : getVerdict(sub[2]),
                    language : getLanguage(sub[5]),
                    runtime : (sub[3] / 1000).toFixed(3),
                    submissionTime : timeConverter(sub[4])
                }
                
                mySubmissiondata.unshift(eachSubmission);
                
            });

            setMySubmissionTable(mySubmissiondata);

            /// Third request ///

            const response3 = await axios.get(`https://uhunt.onlinejudge.org/api/p/rank/${response.data.pid}/1/100`);

            const bestSubmission = [];

            response3.data.forEach(sub => {

                let eachSubmission = {

                    key : sub.sid,
                    submissionsId : sub.sid,
                    rank : sub.rank !== -1 ? sub.rank : '-',
                    userName : `${sub.name}(${sub.uname})`,
                    language : getLanguage(sub.lan),
                    runtime : (sub.run / 1000).toFixed(3),
                    submissionTime : timeConverter(sub.sbt)

                }
                bestSubmission.push(eachSubmission);

            });

            setbestSubmissionTable(bestSubmission);


            /// four request /// 
            const timeNow = Math.floor(new Date() / 1000);
            const timeFeforTenDays = timeNow - 2592000;

            //console.log(timeFeforTenDays + " " + timeNow)

            const response4 = await axios.get(`https://uhunt.onlinejudge.org/api/p/subs/${response.data.pid}/${timeFeforTenDays}/${timeNow}`);

            const lastSubmissiondata = [];

            for(let i = 0 ; i < response4.data.length ; ++ i){

                if(i >= 50) break;

                let sub = response4.data[i];

                let eachSubmission = {
                    key : sub.sid,
                    submissionsId : sub.sid,
                    rank : sub.rank !== -1 ? sub.rank : '-',
                    userName : `${sub.name}(${sub.uname})`,
                    verdict : getVerdict(sub.ver),
                    language : getLanguage(sub.lan),
                    runtime : (sub.run / 1000).toFixed(3),
                    submissionTime : timeConverter(sub.sbt)
                }
                
                lastSubmissiondata.unshift(eachSubmission);

            };
            console.log(lastSubmissiondata.length)

            setLastFiveDaySubmission(lastSubmissiondata);
            
        };

        if(searechProblemNumber !== undefined ) fatchProblemData(313608 , searechProblemNumber);
        
    }, [searechProblemNumber]);

    const getVerdict = (verdictId) => {

        switch(verdictId) {

            case 10: return "Submission error";  
            case 15: return "Can't be judged";
            case 20: return "In queue"; 
            case 30: return "Compile error"; 
            case 35: return "Restricted function"; 
            case 40: return "Runtime error"; 
            case 45: return "Output limit"; 
            case 50: return "Time limit"; 
            case 60: return "Memory limit"; 
            case 70: return "Wrong answer"; 
            case 80: return "PresentationE"; 
            case 90: return "Accepted"; 
            default : return "--------"; 
        }

    };
    
    const getLanguage = (languageId) => {

        switch(languageId) {

            case 1: return "ANSI C";  
            case 2: return "Java";
            case 3: return "C++"; 
            case 4: return "Pascal"; 
            case 5: return "C++11"; 
            case 6: return "Python"; 
            default : return "--------"; 
        }
    }

    const timeConverter = (timestamp) => {
        
        let a = new Date(timestamp * 1000);
        let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        let year = a.getFullYear();
        let month = months[a.getMonth()];
        let date = a.getDate();
        let hour = a.getHours();
        let min = a.getMinutes();
        let sec = a.getSeconds();
        let time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec ;
        
        return time;
    };

    const showSearchData = (value) => {

        setShowResutl(true);
        setsearechProblemNumber(value);
    } 


    const mySubmissionColumns = [
        {
            title: "submission Id",
            dataIndex: "submissionsId",
            key: "SubmissionId",
        },
        {
            title: "Rank",
            dataIndex: "rank",
            key: "rank"
        },
        {
            title: "User Name",
            dataIndex: "userName",
            key: "userName"
        },
        {
            title: "Verdict",
            key: "verdict",
            dataIndex: "verdict",
            render: verdict => (
            <>    
                { 
                    <Tag color= 
                        { 
                            verdict === 'Accepted'? '#228B22' : 
                            verdict === 'PresentationE'? '#666600' :
                            verdict === 'Wrong answer'? '#FF0000' :
                            verdict === 'Time limit'? '#0000FF' :
                            verdict === 'Memory limit'? '#0000AA' :
                            verdict === 'Compile error'? '#AAAA00' :
                            verdict === 'Runtime error'? '#00CED1' :
                            verdict === 'Output limit'? '#000000' : '#000000'

                        } key={verdict}>

                        {verdict.toUpperCase()}
                    </Tag>
                }
            </>
            )
        },
        {
            title: "Language",
            dataIndex: "language",
            key: "language"
        },
        {
            title: "Time",
            dataIndex: "runtime",
            key: "runtime"
        },
        {
            title: "Submission Time",
            dataIndex: "submissionTime",
            key: "submissionTime"
        }
    ];



    const bestSubmissionColumns = [

        {
            title: "submission Id",
            dataIndex: "submissionsId",
            key: "SubmissionId",
        },
        {
            title: "Rank",
            dataIndex: "rank",
            key: "rank"
        },
        {
            title: "User Name",
            dataIndex: "userName",
            key: "userName"
        },
        {
            title: "Language",
            dataIndex: "language",
            key: "language"
        },
        {
            title: "Time",
            dataIndex: "runtime",
            key: "runtime"
        },
        {
            title: "Submission Time",
            dataIndex: "submissionTime",
            key: "submissionTime"
        }


    ]

    return (
        <>

            <Card title="Search Problem" bordered={false} style={{marginBottom : 20}} >
                <Search
                    placeholder="Problem Number"
                    onSearch={ value => showSearchData(value) }
                    style={{ width: 250  }}
                />
            </Card>
            { showResutl ?
                <> 
                <Card title='Problem Details' size='small' headStyle={{ 
                            
                            background: '#1890ff' , 
                            textTransform: 'uppercase' , 
                            fontWeight: 'bold',
                            color: '#fff'

                        }}style={{ marginBottom : 15}}>
                    <Row gutter={[16, 8]}>
                        
                        <Col span={12} >
                        
                            <Divider orientation="left" style={{fontSize : 20}}> {`${problemNumber} - ${problemName}`}</Divider>
                            <List size="small"  
                                    bordered dataSource={[
                                        `Total User Solve : ${solveByUser}`,
                                        `Best Run Time : ${bestSolveTime}`,
                                        `Problem Run-Time Limit : ${problemTime}`,
                                        `Problem Status : ${problemStatus}`,
                                ]} 
                            renderItem={item => <List.Item>{item}</List.Item>}/>
                        
                        </Col>
                        <Col span={12} > 
                            <Card size='small' bordered >
                                <ProblemVerdictStatics verdictCount={verdictCount} problemNumber={problemNumber} problemName={problemName}/>  
                            </Card>
                        </Col>
                            
                    </Row>
                </Card>
                <Card title='Latest Submission' size='small' style={{ marginBottom : 15 }}
                    headStyle={{ 
                        background: '#1890ff' , 
                        textTransform: 'uppercase' , 
                        fontWeight: 'bold',
                        color: '#fff'
                    }}
                >
                    <Table columns={mySubmissionColumns} dataSource={lastFiveDaySubmission} bordered size='small'/>
                </Card>
                
                    <Row gutter={[16, 8]}>
                        <Col span={12} > 
                            <Card title='Your submissions' size='small' 
                                headStyle={{ 
                                    background: '#1890ff' , 
                                    textTransform: 'uppercase' , 
                                    fontWeight: 'bold',
                                    color: '#fff'
                                }}
                            >
                                <Table columns={mySubmissionColumns} size='small' dataSource={mySubmissionTable}   /> 
                            </Card>
                        </Col>
                        <Col span={12} >
                            <Card title='Best submissions' size='small'
                                headStyle={{ 
                                    background: '#1890ff' , 
                                    textTransform: 'uppercase' , 
                                    fontWeight: 'bold',
                                    color: '#fff'
                                }}
                            > 
                                <Table columns={bestSubmissionColumns} size='small' dataSource={bestSubmissionTable}    />
                            </Card>
                        </Col>
                    </Row>
                
                
                </>
                : <> </> 
            
            }
             
        </>
    )
}

export default Allproblems
