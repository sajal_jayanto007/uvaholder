import React , { useState , useEffect } from 'react'

import axios from 'axios';

import {  Table , Tag  , Row , Col} from 'antd';



const Livesubmission = ({ allProblemList }) => {
    
    const [ liveSubmissions , setLiveSumbission ] = useState([]);
    const [ lastPollId , setLastPollId ] = useState(0);


    const getVerdict = (verdictId) => {

        switch(verdictId) {

            case 10: return "Submission error";  
            case 15: return "Can't be judged";
            case 0: return "In queue"; 
            case 30: return "Compile error"; 
            case 35: return "Restricted function"; 
            case 40: return "Runtime error"; 
            case 45: return "Output limit"; 
            case 50: return "Time limit"; 
            case 60: return "Memory limit"; 
            case 70: return "Wrong answer"; 
            case 80: return "PresentationE"; 
            case 90: return "Accepted"; 
            default : return "--------"; 
        }

    };

    const getLanguage = (languageId) => {

        switch(languageId) {

            case 1: return "ANSI C";  
            case 2: return "Java";
            case 3: return "C++"; 
            case 4: return "Pascal"; 
            case 5: return "C++11"; 
            case 6: return "Python"; 
            default : return "--------"; 
        }
    }

    const timeConverter = (timestamp) => {
        
        let a = new Date(timestamp * 1000);
        let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        let year = a.getFullYear();
        let month = months[a.getMonth()];
        let date = a.getDate();
        let hour = a.getHours();
        let min = a.getMinutes();
        let sec = a.getSeconds();
        let time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec ;
        
        return time;
    };
    
    
    useEffect(() => {

        const findProblemByProblemId = (problemId) => { 

            let low = 0 ;
            let high = allProblemList.length;
            let mid = 0;
    
            while(low <= high){
    
                mid = (low + high) >> 1;
    
                if(allProblemList[mid][0] === problemId) return mid;
                else if(allProblemList[mid][0] < problemId)low = mid + 1;
                else high = mid - 1;
            }
    
            return 0;
        };

        const fatchlivesubmission = async (lastPollId) => {

            const response = await axios.get(`https://uhunt.onlinejudge.org/api/poll/${lastPollId}` , { timeout: 60000 });

            let len = response.data.length;

            if(len >= 1) {

                let submissions = [] ;

                response.data.forEach(sub => {

                    let index = -1;
                    let low = 0 ;
                    let len = liveSubmissions.length - 1;

                    while(low <= len){

                        if(sub.msg.sid == liveSubmissions[low].key){
                            index = low ;
                            break;
                        }
                        low++;
                    }
                    
                    if(index === -1){

                        let indexproblem = findProblemByProblemId(sub.msg.pid);
                    
                        let eachSubmission = {
                        
                            key : sub.msg.sid,
                            submissionsId : sub.msg.sid,
                            numberWithName : `${allProblemList[indexproblem][1]} - ${allProblemList[indexproblem][2]}`,
                            userName : `${sub.msg.name}(${sub.msg.uname})`,
                            verdict : getVerdict(sub.msg.ver),
                            language : getLanguage(sub.msg.lan),
                            runtime : (sub.msg.run / 1000).toFixed(3),
                            bestRuntime : (allProblemList[indexproblem][4] / 1000).toFixed(3),
                            rank : sub.msg.rank !== -1 ? sub.msg.rank : '-',
                            submissionTime : timeConverter(sub.msg.sbt)
                        }

                        liveSubmissions.unshift(eachSubmission);

                        if(liveSubmissions.length >= 100) liveSubmissions.pop();
                    }
                    else {

                        let indexproblem = findProblemByProblemId(sub.msg.pid);
                    
                        let eachSubmission = {
                        
                            key : sub.msg.sid,
                            submissionsId : sub.msg.sid,
                            numberWithName : `${allProblemList[indexproblem][1]} - ${allProblemList[indexproblem][2]}`,
                            userName : `${sub.msg.name}(${sub.msg.uname})`,
                            verdict : getVerdict(sub.msg.ver),
                            language : getLanguage(sub.msg.lan),
                            runtime : (sub.msg.run / 1000).toFixed(3),
                            bestRuntime : (allProblemList[indexproblem][4] / 1000).toFixed(3),
                            rank : sub.msg.rank !== -1 ? sub.msg.rank : '-',
                            submissionTime : timeConverter(sub.msg.sbt)
                        }

                        liveSubmissions[index] = eachSubmission;

                    }

                });
               

                setLiveSumbission([ ...liveSubmissions ]);
                setLastPollId(response.data[len - 1].id);

                console.log(submissions);
            }
            else {
                await fatchlivesubmission(lastPollId);
            }
            
        };

        if(allProblemList.length > 0) fatchlivesubmission(lastPollId);

    }, [allProblemList, lastPollId]);

    
    const columns = [
        {
            title: "submission Id",
            dataIndex: "submissionsId",
            key: "submissionId",
        },
        {
            title: "Problem",
            dataIndex: "numberWithName",
            key: "numberWithName",
            render: text => <span>{text}</span>
        },
        {
            title: "User Name",
            dataIndex: "userName",
            key: "userName"
        },
        {
            title: "Verdict",
            key: "verdict",
            dataIndex: "verdict",
            render: verdict => (
            <>    
                { 
                    <Tag color= 
                        { 
                            verdict === 'Accepted'? '#228B22' : 
                            verdict === 'PresentationE'? '#666600' :
                            verdict === 'Wrong answer'? '#FF0000' :
                            verdict === 'Time limit'? '#0000FF' :
                            verdict === 'Memory limit'? '#0000AA' :
                            verdict === 'Compile error'? '#AAAA00' :
                            verdict === 'Runtime error'? '#00CED1' :
                            verdict === 'Output limit'? '#000000' : '#000000'

                        } key={verdict}>

                        {verdict.toUpperCase()}
                    </Tag>
                }
            </>
            )
        },
        {
            title: "Language",
            dataIndex: "language",
            key: "language"
        },
        {
            title: "Time",
            dataIndex: "runtime",
            key: "runtime"
        },
        {
            title: "Best",
            dataIndex: "bestRuntime",
            key: "bestRuntime"
        },
        {
            title: "Rank",
            dataIndex: "rank",
            key: "rank"
        },
        {
            title: "Submission Time",
            dataIndex: "submissionTime",
            key: "submissionTime"
        }
    ];
    
    
    return (
        <> 
            <Table columns={columns} dataSource={liveSubmissions} bordered size='small' />
        </>    
    )
}

export default Livesubmission
