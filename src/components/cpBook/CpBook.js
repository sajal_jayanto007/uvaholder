import React , { useState , useEffect } from 'react'

import axios from 'axios';


import { Collapse , Row , Col, Card , Divider , List , Button , Checkbox } from "antd";

const { Panel  } = Collapse;

const name = 100;

function callback(key) {
  console.log(key);
}

const data = [
    'Racing car sprays burning fuel into crowd.',
    'Japanese princess to wed commoner.',
    'Australian walks 100km after outback crash.',
    'Man charged over missing wedding girl.',
    'Los Angeles battles huge wildfires.',
];

export const CpBook = ({ allProblemList }) => {

    const [ problemsBook , setProblemBook ] = useState([]);
    const [ bookChapters , setBookChapters ] = useState([]);
 
    useEffect(() => {


        let ProblemsList = [];


        const findProblemByProblemNumber = (problemNumber) => { 

            let low = 0 ;
            let high = ProblemsList.length;
            let mid = 0;

            while(low <= high){
    
                mid = (low + high) >> 1;
    
                if(ProblemsList[mid][1] === problemNumber) return mid;
                else if(ProblemsList[mid][1] < problemNumber) low = mid + 1;
                else high = mid - 1;
            }

            return -1;
        };

        const fatchCp3Book = async (userId) => {

            const request = await axios.get(`https://uhunt.onlinejudge.org/api/cpbook/1`);

            const solveRequest = await axios.get(`https://uhunt.onlinejudge.org/api/solved-bits/${userId}`);

            console.log(solveRequest.data[0].solved[0]);

            ProblemsList = allProblemList;

            ProblemsList.sort((one , two) => {
                return one[1] - two[1];
            })

            //console.log(ProblemsList)

            let chapters = [];

            for(let i = 0 ; i < request.data.length ; ++i){

                let row = {};
                row["id"] = i;
                row["name"] = request.data[i]["title"];
                
                let category = [];
                for(let j = 0 ; j < request.data[i]["arr"].length ; ++j){
                   
                    let subCategory = [];
                    for(let k = 0 ; k < request.data[i]["arr"][j]["arr"].length ; ++k){
                                         
                        let listProblem = [];
                        let each = {}

                        for(let l = 1 ; l < request.data[i]["arr"][j]["arr"][k].length ; ++l){

                            let number = Math.abs(request.data[i]["arr"][j]["arr"][k][l]);
                            let index = findProblemByProblemNumber(number);

                           // if(index == -1) console.log(number + "   " + index);
                            let problemId = ProblemsList[index][0];
                           
                            let indxmod = parseInt(problemId % 32);

                            console.log(`${number} - ${problemId} - ${parseInt(problemId / 32)}   - ${indxmod}` )
                           
                            let bitOn = Boolean(solveRequest.data[0].solved[parseInt(problemId / 32)] & (1 << indxmod));
                            
                            listProblem.push({ name :` ${number} - ${ProblemsList[index][2]}` , status : bitOn , solve : ProblemsList[index][3] });
                        }

                        each["id"] = k ;
                        each["name"] = request.data[i]["arr"][j]["arr"][k][0];
                        each["problems"] = listProblem;

                        subCategory.push(each);
                       
                        // list_arr = [ ...list_arr , ...request.data[i]["arr"][j]["arr"][k] ];
                    }
                    
                    let rowin = {
                        id : j,
                        name : request.data[i]["arr"][j]["title"],
                        subCategorys : subCategory,
                    };

                    category.push(rowin);
                }

                row["category"] = category ;
                row["listname"] = category[0].subCategorys[0].name;
                row["showList"] = category[0].subCategorys[0].problems;

                chapters.push(row);
            }
            setBookChapters(chapters);
        };
        
        if(allProblemList.length > 0) fatchCp3Book(313608);
        

    },[allProblemList] );


    const showData = (chapterIndex, name , problems) => {

        //console.log(name);
        
        let tempChapters = bookChapters;

        tempChapters[chapterIndex].showList = problems;
        tempChapters[chapterIndex].listname = name;
        
        setBookChapters([ ...tempChapters ]);

    }

    return (
        <>
            <Card bordered>
                <Divider orientation="left"> Competitive Programming 3 </Divider>
                <Collapse bordered onChange={callback}>
                    
                    {   
                        bookChapters.map( (chapter) => 
                            <Panel header={chapter.name} key={chapter.id} style={{paddingBottom: 2}}>
                                <Row gutter={[8, 8]} >
                                    <Col span={8} >
                                        {
                                            chapter.category.map((category) =>
                                                
                                                <Collapse bordered onChange={callback}>
                                                    <Panel header={category.name} key={category.id} >
                                                        <List dataSource={category.subCategorys} renderItem =
                                                            { 
                                                                subCategory => 
                                                                <List.Item> 
                                                                    <Button block onClick={() => showData(chapter.id, subCategory.name, subCategory.problems) } > {subCategory.name} </Button>
                                                                </List.Item>
                                                            } 
                                                        />
                                                    </Panel>
                                                </Collapse>               
                                            )
                                        }
                                    </Col>
                                    <Col span={16} >  
                                        <Divider orientation="left">{ chapter.listname }</Divider>
                                        
                                        <List size="small" bordered dataSource={chapter.showList} renderItem={item => 
                                            
                                            <List.Item>
                                                <Checkbox checked={item.status} > </Checkbox>
                                                {item.name}
                                                
                                            </List.Item>

                                        } />
                                    </Col>
                                </Row>
                            </Panel>
                        )
                    }

                </Collapse>
            </Card>
        </>
    );
}


export default CpBook