import React , { useState , useEffect } from 'react'

import axios from 'axios';

import { Table   } from 'antd';

export const Ranklist = () => {

    const [ ranklist , setRankList ] = useState([]);

    useEffect(() =>{


        const fatchRanklist = async (userId , upLimet, downLimet) => {

            const response = await axios.get(`https://uhunt.onlinejudge.org/api/ranklist/${userId}/${upLimet}/${downLimet}`);

            let rank = [];

            response.data.forEach(sub => {

                let temprank = {

                    rank : sub.rank,
                    username : sub.username,
                    name : sub.name,
                    totalSubmission : sub.nos,
                    solved : sub.ac,
                    last2Days : sub.activity[0],
                    last7Days : sub.activity[1],
                    last31Days : sub.activity[2],
                    last3months : sub.activity[3],
                    last1year : sub.activity[4],
                }

                rank.push(temprank);

            });

            setRankList(rank);
        }

        fatchRanklist(339 , 10 , 10);

    }, []);

    const columns = [

        {
            title: "Rank",
            dataIndex: "rank",
            key: "rank",
        },
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
        },
        {
            title: "username",
            dataIndex: "username",
            key: "username",
        },
        {
            title: "Solved",
            dataIndex: "solved",
            key: "solved",
        },
        {
            title: "Submissions",
            dataIndex: "totalSubmission",
            key: "totalSubmission",
        },
        {
            title: "2 days",
            dataIndex: "last2Days",
            key: "last2Days",
        },
        {
            title: "7 days",
            dataIndex: "last7Days",
            key: "last7Days",
        },
        {
            title: "31 days",
            dataIndex: "last31Days",
            key: "last31Days",
        },
        {
            title: "3 Months",
            dataIndex: "last3months",
            key: "last3months",
        },
        {
            title: "1 Years",
            dataIndex: "last1year",
            key: "last1year",
        },

    ];


    return (
        <>
            <Table columns={columns} dataSource={ranklist} bordered size="small"/>
        </>
    );
}



export default Ranklist;