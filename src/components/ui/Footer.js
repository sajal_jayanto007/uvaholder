import React from 'react'
import { Layout } from 'antd'

const { Footer } = Layout;

const MyFooter = () => {
    return (
        <Footer style={{ textAlign: 'center' , backgroundColor : '#fff'}}> Uva Holder © 2020 Created by Jayanto Mondal</Footer>
    )
}

export default MyFooter
