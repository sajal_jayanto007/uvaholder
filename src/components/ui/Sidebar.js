import React from 'react'
import { DesktopOutlined, PieChartOutlined, FileOutlined, TeamOutlined, UserOutlined } from '@ant-design/icons';
import { Layout , Menu } from 'antd';
import { Link } from 'react-router-dom';

import {BrowserRouter as Router , Switch , Route } from 'react-router-dom';

const { SubMenu } = Menu;
const { Sider } = Layout;



const Sidebar = () => {
    return (
        <Sider collapsible  style={{ minHeight: '100vh'}}>
            <div className="logo"> Uva Holder </div>
            <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                
                <Menu.Item key="1" icon={<PieChartOutlined />}> 
                    <span>Deshboard</span>
                    <Link to="/" /> 
                </Menu.Item>
                
                <Menu.Item key="2" icon={<DesktopOutlined />}> 
                    <span>My Submissions</span>
                    <Link to="/submission" /> 
                </Menu.Item>

                <Menu.Item key="3" icon={<DesktopOutlined />}> 
                    <span>Live Submission</span>
                    <Link to="/livesubmission" /> 
                </Menu.Item>

                <Menu.Item key="4" icon={<DesktopOutlined />}> 
                    <span>Search Problem </span>
                    <Link to="/allproblems" /> 
                </Menu.Item>

                <Menu.Item key="5" icon={<DesktopOutlined />}> 
                    <span> Past Contest </span>
                    <Link to="/contest" /> 
                </Menu.Item>

                <Menu.Item key="6" icon={<DesktopOutlined />}> 
                    <span> Rank </span>
                    <Link to="/ranklist" /> 
                </Menu.Item>

                
                <SubMenu key="cpbook" icon={<UserOutlined />} title="CP Book">
                    
                    <Menu.Item key="cpbook1"> 
                        <span> CP Book 1 </span>
                        <Link to="/cpbook3" /> 
                    </Menu.Item>

                    <Menu.Item key="cpbook2"> 
                        <span>CP Book 2 </span>
                        <Link to="/cpbook3" />    
                    </Menu.Item>

                    <Menu.Item key="cpbook3"> 
                        <span> CP Book 3 </span>
                        <Link to="/cpbook3" />
                    </Menu.Item>
                
                </SubMenu>

                <Menu.Item key="9" icon={<FileOutlined />} > 
                    <span> comper Solve </span>
                    <Link to="/compersolve" />
                </Menu.Item>

                <SubMenu key="sub2" icon={<TeamOutlined />} title="Team">
                    <Menu.Item key="6">Team 1</Menu.Item>
                    <Menu.Item key="8">Team 2</Menu.Item>
                </SubMenu>

            </Menu>
        </Sider>
    )
}

export default Sidebar
   