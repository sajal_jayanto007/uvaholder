import React , { useState , useEffect } from 'react'

import axios from 'axios';


import { Card , Input , Space , Button , List , Tag } from 'antd'

const ComperSolve = ({ allProblemList }) => {

    const [ isLoading , setisLoading ] = useState(true);
    const [ comperWith , setComperWith ] = useState("");
    const [ comperTo , setComperTo ] = useState("");
    const [ comperNow , setComperNow ] = useState({});
    const [ comperProblemList , setComperProblemList ] = useState([]);


    useEffect(() => {

        if(allProblemList.length > 0){
            setisLoading(false);
        }

    } , [allProblemList]);



    useEffect(() => {

        const findProblemByProblemId = (problemId) => { 

            let low = 0 ;
            let high = allProblemList.length;
            let mid = 0;
    
            while(low <= high){
    
                mid = (low + high) >> 1;
    
                if(allProblemList[mid][0] === problemId) return mid;
                else if(allProblemList[mid][0] < problemId)low = mid + 1;
                else high = mid - 1;
            }
    
            return 0;
        };


        const fetchData = async (comperWithId, comperToId) => {

            const response3 = await axios.get(`https://uhunt.onlinejudge.org/api/solved-bits/${comperWithId},${comperToId}`);

            let comperWithData = response3.data[0].solved;
            let compertoData = response3.data[1].solved;

            let problemList = [];

            for(let i = 1 ; i < comperWithData.length ; ++i){
                for(let j = 0 ; j < 32 ; ++j){
                    
                    let problemId =  (i * 32) + j;
                    let bitOn = (comperWithData[i] & (1 << j));

                    if(bitOn) {
                       
                        let bitOn2 = (compertoData[i] & (1 << j));
                        let index = findProblemByProblemId(problemId);
                        if(!bitOn2) {
                            problemList.push(`${allProblemList[index][1]} ${allProblemList[index][2]}`);
                        }
                        
                    }
                }
            }

            setComperProblemList([ ...problemList ]);

        } 

        const fatchID = async () => {

            const response = await axios(
                { 
                    method:'get', 
                    url:`https://uhunt.onlinejudge.org/api/uname2uid/${comperNow.with}` 
                } 
            );

            const response2 = await axios(
                { 
                    method:'get', 
                    url:`https://uhunt.onlinejudge.org/api/uname2uid/${comperNow.to}`
                }
            );
            
            fetchData(response.data , response2.data);
        } 

        
        if(!isLoading) {

            setisLoading(true);
            fatchID();
            setisLoading(false);
        }
        

    }, [comperNow]);


    const ComperButtonClick = () => {

        let comperUname = {
            with : comperWith.replace(/\s+/g, ''),
            to : comperTo.replace(/\s+/g, '')
        }

        console.log(comperUname);
        setComperNow(comperUname);
    };

    const ClearInput = () => {

        setComperWith("");
        setComperTo("");
    }; 

    return (
        <>
            <Card title="Comper Solve Count" bordered={false} style={{marginBottom : 10}} >
                
                <Space direction="horizontally">

                    <Input placeholder="Comper with" value={comperWith} onChange={(e) => setComperWith(e.target.value)} />
                    <Input placeholder="Comper To" value={comperTo} onChange={(e) => setComperTo(e.target.value)} />
                    <Button type="primary" loading={isLoading} onClick={ComperButtonClick}> Comper </Button>
                    <Button type="primary" onClick={ClearInput}> Clear </Button>

                </Space>

            </Card>

            <Card title="Comper" bordered={false} style={{marginTop : 10}}>
                <List grid={{  gutter: 16, xs: 3, sm: 5, md: 5, lg: 6, xl: 8, xxl: 10 }}
                    dataSource={comperProblemList}
                    renderItem={item => (
                        <List.Item>
                            <Card size='small'>{item}</Card>
                        </List.Item>
                    )}
                />
            </Card>

        </>
    );
}

export default ComperSolve
