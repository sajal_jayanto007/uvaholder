import React , { useState , useEffect} from 'react';
import './App.css';
import Sidbar from './components/ui/Sidebar';
import Header from './components/ui/Header';
import Footer from './components/ui/Footer';

import {BrowserRouter as Router , Switch , Route } from 'react-router-dom';

import Allsubmission from './components/submissions/Allsubmission';
import Dashbord from './components/dashbord/Dashbord';
import Allproblems from './components/problems/Allproblems';

import Livesubmission from './components/live-submissions/Livesubmission';

import ComperSolve from './components/comperSolve/ComperSolve';

import Ranklist from './components/rankList/Ranklist';

import CpBook from './components/cpBook/CpBook';

import Contest from './components/contests/Contest';

import axios from 'axios';

import { Layout } from 'antd';
const { Content } = Layout;

const App = () => {

    const [ allProblemList , setAllProblemList ] = useState([]);
    
    useEffect(() => {
        
        const fatchAllProblems = async () => {
            
            const response = await axios.get(`https://uhunt.onlinejudge.org/api/p`);
            
            setAllProblemList(response.data);
        }

        fatchAllProblems();
    }, []);

    return (
        <Router>
        <Layout>
            <Sidbar />
            <Layout className="site-layout" >
                <Header />
                
                    <Content style={{ margin: '20px 16px'  , overflow: 'initial' }}>
                        <div  style={{ padding: 24  }}>
                            <Switch>

                                <Route path="/" exact >
                                    <Dashbord/>
                                </Route>
                                
                                <Route path="/submission" > 
                                    <Allsubmission allProblemList={allProblemList}/>
                                </Route>

                                <Route path="/allproblems" > 
                                    <Allproblems  />
                                </Route>

                                <Route path="/livesubmission" > 
                                    <Livesubmission allProblemList={allProblemList}/>
                                </Route>

                                <Route path="/compersolve" > 
                                    <ComperSolve allProblemList={allProblemList} /> 
                                </Route>

                                <Route path="/cpbook3" > 
                                    <CpBook allProblemList={allProblemList}/> 
                                </Route>

                                <Route path="/ranklist" > 
                                    <Ranklist/>
                                </Route>

                                <Route path="/contest" > 
                                    <Contest allProblemList={allProblemList} />
                                </Route>

                            </Switch>
                        </div>
                    </Content>
                
                <Footer/>
            </Layout>
        </Layout>
        </Router>
    );
}

export default App;
